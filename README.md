# Conversational AI - Research Project

This repo contains our research notes and some other useful contents for building an advanced and configurable conversational AI models

## Features of the proposed conversational AI model

------------

- ### Multi persona end2end model Icecaps

ICECAPS Capabilities include multi-task learning between models with shared parameters, upgraded language model decoding features, a range of built-in architectures, and a user-friendly data processing pipeline. The system is targeted toward conversational tasks, exploring diverse response generation, coherence, and knowledge grounding.

- ### External knowledge

The key idea is to provide the conversation model with relevant long-form text on the fly as a source of external knowledge. The model performs QA-style reading comprehension on this text in response to each conversational turn, thereby allowing for more focused integration of external knowledge than has been possible in prior approaches.

## Useful Links

------------

- [Natural Language Processing Best Practices & Examples](https://github.com/microsoft/nlp)  Some examples of NLP tasks

- [Github Conversational AI](https://conversationai.github.io/)  Github NLP & Conversational AI page

- [Microsoft ICECAPS Repo](https://github.com/microsoft/icecaps)   ICECAPS Multi-persona deep conversational model - Personalization of end2end models

- [Microsoft ICECAPS Blog](https://www.microsoft.com/en-us/research/project/microsoft-icecaps/)

- [Papers on chatbots](https://www.chatbots.org/papers/)   Some papers on the field of Conversational AI

- [Applications of NLP using Modern Methods](https://github.com/omarsar/nlp_overview)   Examples of NLP tasks using modern deep learning methods

## Ali's Notes

------------

### CHATBOT-Architecture Notes

#### Chatbot Evaluation Processes

##### PARADISE Framework: paradigm for dialogue system evaluation

- Maximizing task effectiveness

- Minimizing dialogue costs

##### Dialogue Breadth test

##### Some other evaluation criterias

1. Bots that has human appearance increase user engagement and the willingness of individuals to interact with the bot

2. Does the bot have a unique voice ?

3. Does it have General knowledge of facts or it is just domain specific ?

4. Does it understand the context ? can it lead a coherent dialogue ? Can it repair a "Broken conversation" ?

5. Does it have a rich personality ? (it likes some things and dislike other things)

6. What is the depth of personalization options for the chatbot? (Does it know whom it is talking to ?)

#### Using Natural Language Processing for chatbots

##### Dialogue Act Recognition (DA)

The process of analyzing the function of a sentence:

| Input | Act | Interpretation |
| --- | ---| --- |
| Hello! | Conventional-opening | Starting Conversation |
| My name is Ali | Statement | It might need an answer |
| What is your name ? | Question | Needs an answer |

Note: There are some DA sets like: DAMSL, SWDB-DAMSL, Meeting Recorder, Verbmobil, Map task

##### Intent Classification

We might have some n-predefined options

Many of the same ML algorithms used for DA recognition are used for Intent Classification

##### Information Extraction

To extract information from text, we convert unstructured text into structured grammatical objects, which will be processed by the **Dialogue Manager**

- Some of the basic tools for Information Extraction:

1. Bag of words
2. Latent Semantic Analysis
3. Regular Expressions
4. Part of speech
5. Named entity recognition
6. Semantic Role Labeling
7. Creation of Grammatical Data Structures

- Some of modern statistical methods

1. Hidden Vector State
2. Support Vector Machine
3. Conditional Random Fields (CRF's)
4. Deep Learning approaches (Seq2Seq..)

#### Response Generation

Response Generator recieves a structure representation of the spoken text:

- Who is speaking
- The dialogue history
- The context

The Response Selector has three keys to make a decision for a response:

- Knowledge database/corpus
- Dialogue history corpus
- External knowledge source "common sense knowledge"

##### The ways of retrieving response

- Rule based models
- Information Retrieval models
- SMT Generative models
- Seq2Seq models
- Reinforcement Learning with seq2seq

#### Knowledge Base Creation

Collecting training data used to train machine learning classifiers used in generative bot models or building corpuses of data used by information retrieval bots is critical to achieving human-like interactions

- Human-Annotated Corpora (Dialogue Diversity Corpus)
- Discussion Forums (Reddit, Wikipedia etc..)
- Email Conversations

Some information retrieval tools: Lucene, Indri

#### Dialogue Management

- Communication Strategies
- Language Tricks:
    * Switching Topics
    * Asking Open Questions
    * Tell a Joke
    * Elicit more information
- Dialogue Design Principles
    * Dessolving ambiguities
    * Confirmation
    * Completion
- Human Imitation Strategies
    * Personality Development (Must have personality, name ...)
    * Direct the Conversation (Bots frequently ask questions to elicit user participation and minimize the chance for error)
    * Small Talk (Humans revert to "neural, non-task oriented conversation about safe topic, where no specific goals need to be achieved" For bots to appear human, built-in small-talk functionality rapport with the human and avoids silence)
    * Human-like Failures (Including disfluencies in dialogue makes bots appear human)
